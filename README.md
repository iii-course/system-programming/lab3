# lab3

## Description
This is an education project written in C. It consists of 2 subprograms. The first one registers a new handler for SIGHUP. The second one creates a POSIX shared memory object.

## Installation
To compile each program, simply run one of the following commands:
```sh
make task1
make task2
```

## Usage

### Run
To run the program, run one of the following commands:
```sh
./task1
./task2
```

For task1, send signals to the process by this command: 
```sh
kill -HUP [pid]
```
You will see new data appearing in a log file.

To clean all *.o files, run:
```sh
make clean
```

### Log
You can find log data for task1 in this file: ./log.txt.
