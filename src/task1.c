#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

const char LOG_FILE[8] = "log.txt";
int log_fd = -1;

void signal_handler( int signo, siginfo_t *si, void * ucontext);
int open_for_writing(const char *filename);
void log_process_start(int fd);
void log_process_running(int fd);
void write_formatted_string_to_buffer(char *buffer, const char *format, ...);
void write_process_info_to_buffer(char *buffer);
void write_to_file(int fd, char *message);

int main() {
    log_fd = open_for_writing(LOG_FILE);
    log_process_start(log_fd);

    struct sigaction new_action, old_action;
    new_action.sa_handler = NULL;
    new_action.sa_sigaction = signal_handler;
    sigemptyset(&new_action.sa_mask);
    sigaddset(&new_action.sa_mask, SIGINT);
    new_action.sa_flags = SA_SIGINFO;

    sigaction(SIGHUP, &new_action, &old_action);

    while(1) {
        log_process_running(log_fd);
        sleep(5);
    }
}

int open_for_writing(const char *filename) {
    int fd = open(filename, O_TRUNC | O_CREAT | O_WRONLY, 0644);
    if (fd == -1) {
        perror("Error opening file: ");
        exit(EXIT_FAILURE);
    }
    return fd;
}

void signal_handler(int signo, siginfo_t *si, void *ucontext) {
    if (log_fd != -1) {
        char buffer[512];
        write_formatted_string_to_buffer(
            buffer,
            "\nSignal %d:\n\tsi_signo: %d\n\tsi_code: %d\n\tsi_pid: %d\n\tsi_uid: %d\n\tsi_status: %d",
            signo, si->si_signo, si->si_code, si->si_pid, si->si_uid, si->si_status
        );
        write_to_file(log_fd, buffer);
    }
}

void log_process_start(int fd) {
    write_to_file(fd, (char *)"\nProcess has started");

    char buffer[256];
    write_process_info_to_buffer(buffer);
    write_to_file(fd, buffer);
}

void log_process_running(int fd) {
    write_to_file(fd, (char *)"\nProcess is running...");
}

void write_to_file(int fd, char *message) {
    if (write(fd, message, strlen(message)) == -1) {
        perror("Error writing to file: ");
        exit(EXIT_FAILURE);
    }
}

void write_process_info_to_buffer(char *buffer) {
    pid_t pid = getpid();
    pid_t gpid = getgid();
    pid_t spid = getsid(pid);
    write_formatted_string_to_buffer(
        buffer, 
        "\nProcess ID: %d, process group ID: %d, process session ID: %d", 
        pid, gpid, spid
    );
}

void write_formatted_string_to_buffer(char *buffer, const char *format, ...) {
  va_list args;
  va_start(args, format);
  vsprintf(buffer,format, args);
  va_end(args);
}
