#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>       
#include <unistd.h> 
#include <fcntl.h>     

struct Data {
    pid_t pid;
    time_t time;
    char str[256];
};

char shared_file[7] = "shared";

void write_new_data(struct Data *data, char *str);

int main() {
    printf("\nsession ID: %d", getsid(getpid()));

    int fd = shm_open(shared_file, O_CREAT | O_RDWR, 0644);
    if (fd == -1) {
        perror("\nError opening file: ");
    }

    if (ftruncate(fd, sizeof(struct Data)) == -1) {
        perror("\nError truncating file: ");
    }

    struct Data *data = NULL;
    data = mmap(NULL, sizeof(struct Data), PROT_WRITE, MAP_SHARED, fd, 0);
    if (data == MAP_FAILED) {
        perror("\nError mapping: ");
        return 1;
    }

    write_new_data(data, NULL);

    while (true) {
        printf("\nEnter next string: ");
        char str[256];
        scanf("%255s", str);

        msync(data, sizeof(struct Data), MS_SYNC);

        printf("\nCurrent data: \n\tpid: %d\n\ttime: %ld\n\tstr: %s", data->pid, data->time, data->str);

        printf("\nWriting new data...");
        write_new_data(data, str);
        printf("\nNew data written");
    }
}

void write_new_data(struct Data *data, char *str) {
    if (data != NULL) {
        data->pid = getpid();
        data->time = time(NULL);
        if (str != NULL) {
            sprintf(data->str, "%s", str);  
        } else {
            sprintf(data->str, " ");  
        }
    }
}