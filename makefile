CC=gcc
CFLAGS=-Wall -Werror -g -ggdb
LIBS=-lrt
SRC=src

task1: task1.o
	$(CC) $(CFLAGS) task1.o -o task1

task2: task2.o
	$(CC) $(CFLAGS) task2.o -o task2 $(LIBS)

task1.o: $(SRC)/task1.c
	$(CC) $(CFLAGS) -c $(SRC)/task1.c

task2.o: $(SRC)/task2.c
	$(CC) $(CFLAGS) -c $(SRC)/task2.c

clean:
	rm -rf *.o task1 task2
